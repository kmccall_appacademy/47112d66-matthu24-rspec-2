def reverser(&prc)
  result = []
  prc.call.split.each do |word|
    result << word.reverse
  end
  result.join(" ")
end

def adder(adder = 1, &prc)
  prc.call + adder
end

def repeater(repeater = 1,&prc)
  repeater.times{prc.call}
end
