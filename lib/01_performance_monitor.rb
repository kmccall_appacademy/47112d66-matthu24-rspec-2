def measure(count = 1,&prc)
  duration = 0
  count.times do
    start_time = Time.now
    prc.call
    end_time = Time.now
    duration += end_time - start_time
  end
  duration/count
end
